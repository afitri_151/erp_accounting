<?php

function year($from = 1990, $to = null) {
    
    if ($to==null){
        $to = date('Y');
    }
    
    $year = [];

    for ($i=$from; $i<=$to; $i++) { 
        $year[$i] = $i;
    }

    return $year;
}