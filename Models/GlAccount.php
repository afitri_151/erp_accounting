<?php

namespace App\Apps\Accounting\Models;

use App\Models\Model;

class GlAccount extends Model{
    public $fillable = ["account_code","account_name","group_code"];
	public $table = "gl_account";

    public static $rules = [
    	"account_code" => "required|string",
    	"account_name" => "required|string",
    	"group_code" => "string"
    ];

    protected $primaryKey = "account_code";

    public $timestamps = false;
}
