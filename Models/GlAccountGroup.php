<?php

namespace App\Apps\Accounting\Models;

use App\Models\Model;

class GlAccountGroup extends Model
{
    public $fillable = ["group_code","group_name"];
    public $table = "gl_account_group";

    public static $rules = [
    	"group_code" => "required|string",
    	"group_name" => "required|string"
    ];

    public $timestamps = false;
}
