<?php

namespace App\Apps\Accounting\Models;

use App\Models\Model;

class AccountingPeriod extends Model
{
    public $fillable = ["year","description"];
    public $table = "accounting_period";

    public static $rules = [
    	"year" => "required|integer",
    	"description" => "required|string"
    ];

    protected $primaryKey = "year";

    public $timestamps = false;
}
