<?php

Route::get("/","AkunController@getIndex");

Route::menu([
		"title" => "Konfigurasi",
		"icon" => "fa fa-cogs",
	], function() {
		Route::menu([
				"title"=> "Klasifikasi Akun",
				"icon" => "fa fa-user",
		],function(){
			Route::get("list_akun","AkunController@getIndex")
				->menu("Daftar Akun","fa fa-th-list");
			Route::get("tambah_akun","AkunController@getAdd")
				->menu("Tambah Akun","fa fa-plus");
			Route::get("hapus_akun","AkunController@getIndex")
				->menu("Hapus Akun","glyphicon glyphicon-trash");
		});
		Route::menu([
				"title"=> "Periode Akuntansi",
				"icon" => "fa fa-calendar-o",
		],function(){
			Route::get("periode_akun","PeriodeAkunController@getIndex")
				->menu("Daftar Periode","fa fa-th-list text-info");
			Route::get("tambah_periode","PeriodeAkunController@getAdd")
				->menu("Tambah Periode","fa fa-plus text-success");
			Route::get("hapus_periode","PeriodeAkunController@postDelete")
				->menu("Hapus Periode","glyphicon glyphicon-trash");
		});
		Route::get('saldo_awal', 'SaldoController@getIndex')
			->menu("Saldo Awal", "fa fa-money");

});

Route::menu([
		"title" => "End Of Year",
		"icon" => "fa fa-archive",
	], function() {
	Route::get('tutup_buku', 'TutupBukuController@getIndex')
			->menu("Tutup Buku", "fa fa-book");

});

Route::menu([
		"title" => "Admin",
		"icon" => "fa fa-rocket",
	], function() {
		Route::get('dashboard_customer', 'CustomerController@getIndex')
			->menu("Data Customer", "fa fa-users");
		Route::get('dashboard_employee', 'EmployeeController@getIndex')
			->menu("Data Pegawai", "fa fa-user");
		Route::get('dashboard_vendor', 'VendorController@getIndex')
			->menu("Data Vendor", "fa fa-shopping-cart");
		Route::get('dashboard_jurnal', 'JurnalController@getIndex')
			->menu("Jurnal", "fa fa-file-text-o");
});

// Akun URL
Route::get("akun_data","AkunController@anyTransactionData");
Route::get("akun/create","AkunController@getAdd");
Route::post("akun","AkunController@postAdd");
Route::get("akun/delete/{id}","AkunController@postDelete");

// Periode Akun URL
Route::get("periode_data","PeriodeAkunController@anyTransactionData");
Route::get("periode_akun/create","PeriodeAkunController@getAdd");
Route::get("periode_akun/{id}","PeriodeAkunController@getEdit");
Route::post("periode_akun/{id}","PeriodeAkunController@postEdit");
Route::post("periode_akun","PeriodeAkunController@postAdd");
Route::get("periode_akun/delete/{id}","PeriodeAkunController@postDelete");

Route::controller("akun", "AkunController");
Route::controller("periode_akun", "PeriodeAkunController");