<?php 
	return [
		"name" => "Human Resource", // nama aplikasi
		"db-name" => "hr", // database aplikasi
		"description" => "Aplikasi Human Resource", #optional
		// app dependencies
		"require" => [
			"file-management" => 2,
		],
		// features
		"features" => [
			// key feature
			"pegawai" => [
				"name" => "Data Pegawai", // nama fitur
				"description" => "Informasi Data Pegawai", #optional
				// credentials
				"credentials" => [
					"list-view",
					"print",
					"keluarga",
					"pendidikan terakhir",
					"kontak",
				],
			]
		]
	];

?>