@extends('backend::app')

@section('content')
    <div class='container'>
        <h1>Tambah Jurnal</h1>
 		{!! Form::open(['url'=>'TambahJurnal']) !!}
 		<div class="form-group">
 			{!! Form::group('text', 'no_bukti', 'No. Bukti') !!}
			{!! Form::group('date', 'tanggal', 'Tanggal') !!}
			{!! Form::group('text', 'desc', 'Keterangan') !!}
 		</div>
 		<table class="table table-bordered">
		    <thead>
		    	<tr>
			        <th>Nama Akun</th>
			        <th>Debet</th>
			        <th>Kredit</th>
			        <th>Debet-Kredit</th>
			        <th>Keterangan</th>
			        <th>Aksi</th>
		      	</tr>
		    </thead>
		    <tbody>
		    	<tr>
		    		<td>{!! Form::select('account_name', array(
		    		'Aktiva Lancar',
    				'Kas & Bank',
    				'Kas' => array('Kas Besar' => 'BCA No. 012345'),
					)) !!}</td>
					<td>
						{!! Form::text('debet',null,['class'=>'form-control']) !!}
					</td>c
					<td>
						{!! Form::text('kredit',null,['class'=>'form-control']) !!}
					</td>
					<td>
						{{ Form::label('debet_kredit', '-', ['class' => 'form-control']) }}
					</td>
					<td>
						{!! Form::text('ket',null,['class'=>'form-control']) !!}
					</td>
					<td>
						{!! Form::submit('Save',['class'=>'btn btn-primary']) !!}
					</td>
		    	</tr>
		    </tbody>
	 	</table>
	 	{!! Form::close() !!}
@endsection