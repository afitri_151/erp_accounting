<?php
	use App\Apps\Accounting\Models\GlAccountGroup;
?>

@extends('backend::app')

@section('content')
	<div class="container">
		@section('title','Tambah Akun')

		{!! Form::open(['url'=>'accounting/akun']) !!}

		<div class="form-group">
 			{!! Form::group('text', 'account_code', 'Kode Akun') !!}
			{!! Form::group('text', 'account_name', 'Nama Akun') !!}
			{!! Form::group('select','group_code','Grup Akun',GlAccountGroup::lists("group_name","group_code"))!!}
			<button type="button" data-dismiss="modal" class="btn btn-danger">
				<span class="glyphicon glyphicon-remove"></span>
				Cancel
			</button>
			<button type="submit" class="btn btn-primary">
				<span class="glyphicon glyphicon-floopy-disk"></span>
				Cancel
			</button>
 		</div>
		
		{!! Form::close() !!}
	</div>
@endsection