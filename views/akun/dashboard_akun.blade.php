@extends('backend::app')

@section('content')
	<div class="container">
		@section('title','Daftar Akun')
		<a href="akun/create" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Tambah Akun</a>
		<a href="" class="btn btn-success"><span class="glyphicon glyphicon-cloud-upload"></span> Import</a>
		<!-- <div class="btn-group" id="dropdown-menu">
			<button type="button" class="btn btn-default disabled">Aksi</button>
				<button type="button" class="btn btn-default disabled dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<span class="caret"></span>
				<span class="sr-only">Toggle Dropdown</span>
			</button>
			<ul class="dropdown-menu">
				<li><a href="#">Delete</a></li>
			</ul>
		</div> -->
		<table class="table table-hover table-bordered" datatable="{!! url('accounting/akun_data') !!}">
		    <thead>
		        <tr>
					<th dt-field="account_code" sort="false">Kode</th>
					<th dt-field="account_name">Nama Akun</th>
					<th dt-field="group_name">Group Name</th>
					<th dt-field="action" sort="false" search="false" width="30px">Aksi</th>
		        </tr>
		    </thead>
		    <tbody>
		        
		    </tbody>
		</table>
	</div>
@endsection