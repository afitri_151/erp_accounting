<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE-edge">
		<title></title>
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/app/accounting/JurnalController/bootstrap.min.css') }}">
	</head>
	<body>
		@yield('content')
		<script type="text/javascript" src="{{ URL::asset('js/app/accounting/JurnalController/jquery.min.js') }}"></script>
		<script type="text/javascript" src="{{ URL::asset('js/app/accounting/JurnalController/bootstrap.min.js') }}"></script>
	</body>
</html>