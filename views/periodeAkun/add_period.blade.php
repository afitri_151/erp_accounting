@extends('backend::app')

@section('content')
	@section('title','Tambah Periode Akun')

	{!! Form::open(['url'=>'accounting/periode_akun']) !!}
		<div class="row">
			<div class="col-md-6">
				{!! Form::label('Tahun','Tahun Periode:',['class'=>'col-md-6']) !!}
			</div>
			<div class="col-md-6">
				{!! Form::select('year',year()) !!}
			</div>
		</div>
		<div class="row">
			{!! Form::group('text', 'description', 'Deskripsi') !!}
		</div>
		<div class="row">
			<div class='col-md-10'>
				<button type="submit" class="btn btn-primary">
					<span class="glyphicon glyphicon-floppy-disk"></span>
					Simpan
				</button>
				<button type="button" data-dismiss="modal" class="btn btn-danger">
					<span class="glyphicon glyphicon-remove"></span>
					Cancel
				</button>	
			</div>
		</div>
	{!! Form::close() !!}
@endsection