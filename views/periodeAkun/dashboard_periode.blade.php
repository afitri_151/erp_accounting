@extends('backend::app')
@section('content')
	<div class="container">
		@section('title','Periode Akuntansi')
		<!-- <button modal='#my-modal-selector'>Show Modal</button> -->
		<a href="periode_akun/create" class="btn btn-primary" modal-title="Tambah Periode Akuntansi" modal><span class="glyphicon glyphicon-plus"></span> Tambah Periode</a>
		<table class="table table-responsive table-hover" datatable="{!! url('accounting/periode_data') !!}">
		    <thead>
		        <tr>
					<th dt-field="no" width="20px" sort="false" search="false">No.</th>
					<th dt-field="year" width="60px">Tahun</th>
					<th dt-field="description">Deskripsi</th>
					<th dt-field="action" width="70px">Aksi</th>
		        </tr>
		    </thead>
		    <tbody>
		    </tbody>
		</table>
	</div>
		
@endsection	