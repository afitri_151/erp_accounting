<?php

namespace App\Apps\Accounting\Controllers;

use App\Apps\Accounting\Models\AccountingPeriod;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use DB;
use Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class PeriodeAkunController extends Controller
{
    /*
	* Meload Halaman dashboard Periode Akun
    */
    public function getIndex()
    {
    	return view("periodeAkun.dashboard_periode");
    }

    /*
	* Menampilkan data dalam bentuk ajax (Data Akun, No,year,button aksi(edit delete))
    */

    public function anyTransactionData() 
    {
    	$res = Datatables::of(
			DB::table("accounting_period")->orderBy("year","desc")
		)->make(true);

		$content = $res->getData(); 	
		$content->data = array_map(function($data){
			$no=1;
			$data->action = "<a href='periode_akun/delete/$data->year' class='btn btn-danger' confirm='Apakah anda yakin ?'><span class='glyphicon glyphicon-trash'></span></a> <a href='periode_akun/edit/$data->year' class='btn btn-info' modal-title='Tambah Periode Akuntansi' modal><span class='glyphicon glyphicon-pencil'></span></a>";
			$data->no=$no;
			$no+=1;
			return $data;
		}, $content->data);
		return $res->setData($content);

	}

	public function getAdd(){
		return view("periodeAkun.add_period");
	}

	public function postAdd(Request $request){
		$data = array(
			'year' => Input::get('year'),
			'description' => Input::get('description'),
		);
		AccountingPeriod::create($data);
		return redirect('accounting/periode_akun');
	}

	public function postDelete($id){
		AccountingPeriod::find($id)->delete();
		return redirect('accounting/periode_akun');
	}

	public function getEdit($id){
		$data = AccountingPeriod::find($id);
		return view('periodeAkun.edit_period',compact('data'));
	}

	public function postEdit($id){

	}

}
