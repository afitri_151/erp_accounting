<?php

namespace App\Apps\Accounting\Controllers;

use App\Apps\Accounting\Models\GlAccount;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use DB;
use Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class AkunController extends Controller
{
    /*
	* Meload Halaman dashboard Akun
    */
    public function getIndex()
    {
    	return view("akun.dashboard_akun");
    }    

    /*
	* Menampilkan data dalam bentuk ajax (Data Akun, account_code,account_name,group_name)
    */
    public function anyTransactionData() 
    {
		$res = Datatables::of(
			DB::table("gl_account")
				->join("gl_account_group","gl_account.group_code","=","gl_account_group.group_code")
				->select('account_code','account_name','group_name')
				->orderBy('gl_account_group','desc')
		)->make(true);

		$content = $res->getData(); 	
		$content->data = array_map(function($data){
			$data->action = "<a href='akun/delete/$data->account_code' class='btn btn-danger' confirm='Apakah anda yakin ?'><span class='glyphicon glyphicon-trash'></span></a>";
			return $data;
		}, $content->data);
		return $res->setData($content);

	}

	/*
	* Menampilkan Form Tambah Akun
	*/
	public function getAdd()
	{
		return view("akun.add_akun_form");
	}

	/*
	* Sistem Tambah Akun
	*/
	public function postAdd(Request $request)
	{
		$data = array(
			'account_code' => Input::get('account_code'),
			'account_name' => Input::get('account_name'),
			'group_code' => Input::get('group_code'),
		);
		GlAccount::create($data);
		return redirect('accounting/akun');
	}

	/*
	* Menghapus Data Akun
	*/
	public function postDelete($id){
		GlAccount::find($id)->delete();
    	return redirect('accounting/akun');
	}

}
